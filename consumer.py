import asyncio
import websockets

async def hendler(websocket):
    async for message in websocket:
        print(message)

async def consumer():
    url = 'ws://localhost:8888/live/1'
    async with websockets.connect(url) as websocket:
        await hendler(websocket)

if __name__ == '__main__':
    loop = asyncio.get_event_loop()
    loop.run_until_complete(consumer())
    loop.run_forever()
