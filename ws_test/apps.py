from django.apps import AppConfig


class WsTestConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'ws_test'
