from channels.routing import URLRouter
from django.urls import path

from ws_test.consumer import LiveConsumer

websockets = URLRouter([
    path(
        'live/<int:id>', LiveConsumer,
        name='live_consumer'
    )
])