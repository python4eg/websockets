from channels.generic.websocket import AsyncWebsocketConsumer

class LiveConsumer(AsyncWebsocketConsumer):
    def __init__(self, scope):
        self.scope = scope
        super().__init__()

    async def connect(self):
        self.id = self.scope['url_route']['kwargs']['id']
        self.group_name = f'group-{self.id}'
        await self.channel_layer.group_add(self.group_name,
                                           self.channel_name)

        await self.accept()

    async def receive(self, text_data=None, bytes_data=None):
        print(text_data)

        self.channel_layer.group_send(self.group_name,
                                      'Ok')

    async def send_text(self, event):
        print(event)
        await self.send(text_data='Ok 2')

    async def websocket_disconnect(self, message):
        await self.channel_layer.group_discard(
            self.group_name,
            self.channel_name
        )

    async def __call__(self, receive, send):
        await super().__call__(self.scope, receive, send)