import asyncio
import websockets

async def hendler(websocket):
    async for message in websocket:
        print(message)

async def consumer():
    url = 'ws://localhost:8000'
    async with websockets.connect(url) as websocket:
        await hendler(websocket)

class Server:
    client = []

    async def register(self, sock):
        self.client.append(sock)

    async def unregister(self, sock):
        if sock in self.client:
            self.client.remove(sock)

    async def broadcast(self, message):
        if len(self.client):
            await asyncio.wait([sock.send(message) for sock in self.client])

    async def handler(self, sock, client_addr):
        print(client_addr)
        await self.register(sock)
        try:
            async for message in sock:
                await self.broadcast(message)
        finally:
            await self.unregister(sock)

server = Server()

if __name__ == '__main__':
    start_server = websockets.serve(server.handler, 'localhost', 8000)
    loop = asyncio.get_event_loop()
    loop.run_until_complete(start_server)
    loop.run_forever()
