import asyncio
import websockets

async def producer():
    url = 'ws://localhost:8888/live/1'
    async with websockets.connect(url) as sock:
        await sock.send('Hello. Ali Express Sales. 11.11')
        print(await sock.recv())

if __name__ == '__main__':
    loop = asyncio.get_event_loop()
    loop.run_until_complete(producer())
