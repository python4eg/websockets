from channels.routing import ProtocolTypeRouter

from ws_test.routing import websockets

application = ProtocolTypeRouter({'websocket': websockets})